'use strict';
module.exports = function(sequelize, DataTypes) {
  const Todo = sequelize.define('Todo', {
    title: DataTypes.STRING,
  }, {
    classMethods: {
      associate: (models) => {
        Todo.hasMany(models.TodoItem, {
          foreignKey: 'todoId',
          as: 'todoItems',
        });
      },
    }
  });
  return Todo;
};